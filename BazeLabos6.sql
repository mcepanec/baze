select * from common.korisnici
select * from common.kontakti
select * from common.naselja

--ZAD 1
select
 *
from
    common.naselja n,
    common.zupanije z 
where
    n.idzupanija = z.id

--ZAD 2
select
    n.mjesto,
    n.opcina as op�ina,
    postanskibroj as po�ta,
    z.zupanija as �upanija
from
    common.naselja n,
    common.zupanije z 
where
    n.idzupanija = z.id
    
--ZAD 3
SELECT TO_CHAR(SYSDATE,'DD.MONTHYYYY.') as Danas FROM DUAL

--ZAD 4
select upper(substr(ime,0, 1))||upper(substr(prezime,0,2)) as inicijali, ime, prezime from common.korisnici;

--ZAD 5
select lower(substr(ime,0,1))||lower(translate(prezime, '�Ǝ���枚�', 'CCZSDcczsd')) as username, ime, prezime from common.korisnici;

--ZAD 6
select 
    ime || ' ' || prezime as naziv,
    0 || pozmob || ' ' || mobbro as broj, 
    case spol
    when 0 then
        'mu�ko'
    when 1 then
        '�ensko'
    else
        'nepoznato'
    end as spol
    
from
    common.korisnici kor,
    common.kontakti kon
where
    kon.idkorisnika = kor.id

--ZAD 7
SELECT DISTINCT pozmob
FROM common.kontakti;

--ZAD 8
select 
    pozmob,
    case pozmob
    when 91 then
        'A1'
    when 92 then
        'tomato'
    when 95 then
        'TELE-2'
    when 97 then
        'bonbon'
    end as operater
from 
    common.kontakti
    
--ZAD 9
select 
    spol,
    operater,
from
    common.kontakti kon,
    common.korisnici kor
where
    kon.idkorisnika = kor.id
and
    spol = 0
and
    operater = 'TELE-2'

    