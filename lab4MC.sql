drop Table knjiga;
drop Table Autor;
drop Table Dobavljac;

drop sequence KNJIGA_ID_SEQ;
drop sequence AUTOR_ID_SEQ;
drop sequence DOBAVLJAC_ID_SEQ;


CREATE TABLE knjiga (
	id INT NOT NULL,
	IDautora INT NOT NULL,
	IDdobavljaca INT NOT NULL,
	naslov VARCHAR2(20) NOT NULL,
	signatura VARCHAR2(20) NOT NULL,
	cijena INT NOT NULL,
	napomena VARCHAR2(20) NOT NULL,
	constraint KNJIGA_PK PRIMARY KEY (id));

CREATE sequence KNJIGA_ID_SEQ;

CREATE trigger BI_KNJIGA_ID
  before insert on knjiga
  for each row
begin
  select KNJIGA_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE Autor (
	id INT NOT NULL,
	ime VARCHAR2(255) NOT NULL,
	prezime VARCHAR2(255) NOT NULL,
	constraint AUTOR_PK PRIMARY KEY (id));

CREATE sequence AUTOR_ID_SEQ;

CREATE trigger BI_AUTOR_ID
  before insert on Autor
  for each row
begin
  select AUTOR_ID_SEQ.nextval into :NEW.id from dual;
end;

/
CREATE TABLE Dobavljac (
	id INT NOT NULL,
	ImeDobavljaca VARCHAR2(20) NOT NULL,
	constraint DOBAVLJAC_PK PRIMARY KEY (id));

CREATE sequence DOBAVLJAC_ID_SEQ;

CREATE trigger BI_DOBAVLJAC_ID
  before insert on Dobavljac
  for each row
begin
  select DOBAVLJAC_ID_SEQ.nextval into :NEW.id from dual;
end;

/
ALTER TABLE knjiga ADD CONSTRAINT knjiga_fk0 FOREIGN KEY (IDautora) REFERENCES Autor(id);
ALTER TABLE knjiga ADD CONSTRAINT knjiga_fk1 FOREIGN KEY (IDdobavljaca) REFERENCES Dobavljac(id);

INSERT INTO knjiga (IDautora, IDdobavljaca, naslov, signatura, cijena, napomena) VALUES (7, 7, 'srggf', '2349f8', 12, 'bokkkkkkk');

select * from knjiga;

