--ZAD 1
select * from buses
select * from students
--ZAD 2
SELECT AVG(cijena) FROM buses 

--ZAD 3
select max(cijena) from buses 

--ZAD 4
select min(cijena) from buses

--ZAD 5
select
    *
from 
    students
where 
    AUTOBUS_ID is null
    
--ZAD 6
select
    count(1)
from 
    students
where 
    AUTOBUS_ID is null
    
--ZAD 7
select
    autobus_id,
    ime,
    prezime
from 
    students
INNER JOIN
    buses
ON
    students.autobus_id = buses.id
where 
    AUTOBUS_ID is not null
    
--ZAD 8
select
    count(1)
from 
    students
INNER JOIN
    buses
ON
    students.autobus_id = buses.id
where 
    REG_OZNAKA = 'BJ 345 GH'

--ZAD 9
SELECT
    *
FROM
    students
RIGHT JOIN
    buses
ON
    students.autobus_id = buses.id

--ZAD 10
SELECT
    *
FROM
    students
FULL JOIN
    buses
ON
    students.autobus_id = buses.id

--ZAD 11 
SELECT
    *
FROM
    buses
FULL JOIN
    students
ON
    buses.id = students.autobus_id

--ZAD 12
select
  prezime, count(1)
from 
    students
INNER JOIN
    buses
ON
    students.autobus_id = buses.id
where 
    AUTOBUS_ID is not null
group by 
    prezime
order by 
    count(1) desc
    
--ZAD 13 
select
    prezime
from 
    students
where
    AUTOBUS_ID is not null
group by 
    prezime
order by 
    prezime asc
    
--ZAD 14
select
    prezime,
    ime
from 
    students
where
    AUTOBUS_ID is not null
group by 
    prezime, ime 
order by 
    prezime asc, ime asc 
    
--ZAD 15
select
    ime,
    prezime
from 
    students
where
    AUTOBUS_ID is not null
group by 
    ime, prezime 
order by 
    ime asc, prezime asc

--ZAD 16
select bus.reg_oznaka, count(1) from students stu inner join buses bus on stu.autobus_id = bus.id
group by bus.reg_oznaka
order by count(1) desc

--ZAD 17
select bus.reg_oznaka, count(1), bus.cijena from students stu inner join buses bus on stu.autobus_id = bus.id
group by bus.reg_oznaka, bus.cijena
order by count(1) desc

--ZAD 18
select bus.reg_oznaka, count(1), bus.cijena from students stu inner join buses bus on stu.autobus_id = bus.id
group by bus.reg_oznaka, bus.cijena having count(1) > 4
order by count(1) desc

--ZAD 19
select bus.reg_oznaka, count(1), bus.cijena, bus.cijena*count(1) as ukupno from students stu inner join buses bus on stu.autobus_id = bus.id
group by bus.reg_oznaka, bus.cijena having count(1) > 4
order by count(1) desc

--ZAD 20
select bus.reg_oznaka, count(1), bus.cijena, bus.cijena*count(1) as ukupno from students stu inner join buses bus on stu.autobus_id = bus.id
group by bus.reg_oznaka, bus.cijena having bus.cijena*count(1) > 1200 
order by count(1) desc
