drop table upisi;
drop table predmeti;
drop table nastavnici;
drop table Studenti;

drop sequence PREDMETI_ID_SEQ;
drop sequence UPISI_ID_SEQ;
drop sequence NASTAVNICI_ID_SEQ;
drop sequence STUDENTI_ID_SEQ;

CREATE TABLE Studenti (
	ID NUMBER(9) NOT NULL,
	JMBAG NUMBER(8) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(20) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1) NOT NULL,
    constraint ck_spol1 CHECK (spol in(0, 1)),
	constraint STUDENTI_PK PRIMARY KEY (ID));

CREATE sequence STUDENTI_ID_SEQ;

CREATE trigger BI_STUDENTI_ID
  before insert on Studenti
  for each row
begin
  select STUDENTI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE Nastavnici (
	ID NUMBER(9) NOT NULL,
	JMBAG NUMBER(8) UNIQUE NOT NULL,
	ime VARCHAR2(20) NOT NULL,
	prezime VARCHAR2(30) NOT NULL,
	email VARCHAR2(30) UNIQUE NOT NULL,
	password VARCHAR2(20) NOT NULL,
	spol NUMBER(1) NOT NULL,
    constraint ck_spol2 CHECK (spol in(0, 1)),
	constraint NASTAVNICI_PK PRIMARY KEY (ID));

CREATE sequence NASTAVNICI_ID_SEQ;

CREATE trigger BI_NASTAVNICI_ID
  before insert on Nastavnici
  for each row
begin
  select NASTAVNICI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE predmeti (
	ID NUMBER(9) NOT NULL,
	IDnastavnika NUMBER(9) NOT NULL,
	sifra VARCHAR2(10) UNIQUE NOT NULL,
	naziv VARCHAR2(30) NOT NULL,
	ects NUMBER(1) NOT NULL,
	semestar NUMBER(1) NOT NULL,
    constraint ck_semestar CHECK (semestar in(1, 2, 3, 4, 5, 6)),
	constraint PREDMETI_PK PRIMARY KEY (ID));

CREATE sequence PREDMETI_ID_SEQ;

CREATE trigger BI_PREDMETI_ID
  before insert on predmeti
  for each row
begin
  select PREDMETI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/
CREATE TABLE upisi (
	ID NUMBER(9) NOT NULL,
	IDpredmeta NUMBER(9) NOT NULL,
	IDstudenta NUMBER(9) NOT NULL,
	datum DATE NOT NULL,
	ocjena NUMBER(1),
    constraint ck_ocjena CHECK (ocjena in(1, 2, 3, 4, 5)),
	constraint UPISI_PK PRIMARY KEY (ID));

CREATE sequence UPISI_ID_SEQ;

CREATE trigger BI_UPISI_ID
  before insert on upisi
  for each row
begin
  select UPISI_ID_SEQ.nextval into :NEW.ID from dual;
end;

/


ALTER TABLE predmeti ADD CONSTRAINT predmeti_fk0 FOREIGN KEY (IDnastavnika) REFERENCES Nastavnici(ID);

ALTER TABLE upisi ADD CONSTRAINT upisi_fk0 FOREIGN KEY (IDpredmeta) REFERENCES predmeti(ID);
ALTER TABLE upisi ADD CONSTRAINT upisi_fk1 FOREIGN KEY (IDstudenta) REFERENCES Studenti(ID);

INSERT into predmeti (IDnastavnika, sifra, naziv, ects, semestar) values (1, '123', 'Baza podataka', 5, 3);
INSERT into predmeti (IDnastavnika, sifra, naziv, ects, semestar) values (3 ,'1234', 'Web programiranje', 376, 3);
INSERT into predmeti (IDnastavnika, sifra, naziv, ects, semestar) values (5, '12345', 'Engleski', 1, 3);

insert into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (1, 2, sysdate, 4);
insert into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (2, 3, sysdate, 4);
insert into upisi (IDpredmeta, IDstudenta, datum, ocjena) values (3, 4, sysdate, 4);
--ne radi al kuzim koncept
select * from predmeti;


select * from nastavnici

